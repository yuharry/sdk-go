package gis

// GetGeoInfoInput get geo info input struct
type GetGeoInfoInput struct {
	APIKey string   `json:"api_key,omitempty"`
	IP     string   `json:"ip,omitempty"`
	IPs    []string `json:"ips,omitempty"`
}
