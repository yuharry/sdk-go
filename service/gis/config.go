package gis

import "os"

var endpoint = os.Getenv("GIS_ENDPOINT")

var apiKey = os.Getenv("GIS_API_KEY")

// SetEndpoint set endpoint
func SetEndpoint(s string) {
	endpoint = s
}

// SetAPIKey set api key
func SetAPIKey(s string) {
	apiKey = s
}
