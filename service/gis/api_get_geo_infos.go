package gis

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// SendV1GeoInfosJSONRequest send v1 geo infos json request
func (s *GIS) SendV1GeoInfosJSONRequest(input *GetGeoInfoInput) (json.RawMessage, error) {
	// New url
	url := s.Host.String() + "/v1/geoinfos.json"

	// New request body
	data, err := jsonex.Marshal(input.IPs)
	if err != nil {
		return nil, err
	}

	// New request
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(data))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")

	// Set api key
	if len(input.APIKey) > 0 {
		req.Header.Set("X-Api-Key", input.APIKey)
	}

	// Send request
	resp, err := s.Client.Do(req)
	if err != nil {
		return nil, err
	}
	respBody, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return nil, err
	}

	return respBody, nil
}

// SendV1GeoInfosRequest send v1 geo infos request
func (s *GIS) SendV1GeoInfosRequest(input *GetGeoInfoInput) (map[string]*GeoInfo, error) {
	// Send json request
	respBody, err := s.SendV1GeoInfosJSONRequest(input)
	if err != nil {
		return nil, err
	}

	// Parse body
	var geoInfos map[string]*GeoInfo
	if err := jsonex.Unmarshal(respBody, &geoInfos); err != nil {
		return nil, err
	}

	return geoInfos, nil
}

// SendV2GeoInfosJSONRequest send v2 geo infos json request
func (s *GIS) SendV2GeoInfosJSONRequest(input *GetGeoInfoInput) (json.RawMessage, error) {
	// New url
	url := s.Host.String() + "/v2/geoinfos.json"

	// New request body
	data, err := jsonex.Marshal(IPsResource{input.IPs})
	if err != nil {
		return nil, err
	}

	// New request
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(data))
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")

	// Set api key
	if len(input.APIKey) > 0 {
		req.Header.Set("X-Api-Key", input.APIKey)
	}

	// Send request
	resp, err := s.Client.Do(req)
	if err != nil {
		return nil, err
	}
	respBody, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return nil, err
	}

	return respBody, nil
}

// SendV2GeoInfosRequest send v2 geo infos request
func (s *GIS) SendV2GeoInfosRequest(input *GetGeoInfoInput) (*GeoInfosResource, error) {
	// Send json request
	respBody, err := s.SendV2GeoInfosJSONRequest(input)
	if err != nil {
		return nil, err
	}

	// Parse body
	geoInfos := new(GeoInfosResource)
	if err := jsonex.Unmarshal(respBody, geoInfos); err != nil {
		return nil, err
	}

	return geoInfos, nil
}
