package gis

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
)

// SendV1GeoInfoJSONRequest send v1 geo info json request
func (s *GIS) SendV1GeoInfoJSONRequest(input *GetGeoInfoInput) (json.RawMessage, error) {
	// New url
	url := s.Host.String() + "/v1/geoinfo.json?ip=" + input.IP

	// New request
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Set("Accept", "application/json")

	// Set api key
	if len(input.APIKey) > 0 {
		req.Header.Set("X-Api-Key", input.APIKey)
	}

	// Send request
	resp, err := s.Client.Do(req)
	if err != nil {
		return nil, err
	}
	respBody, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return nil, err
	}

	return respBody, nil
}

// SendV1GeoInfoRequest send v1 geo info request
func (s *GIS) SendV1GeoInfoRequest(input *GetGeoInfoInput) (map[string]*GeoInfo, error) {
	// Send json request
	respBody, err := s.SendV1GeoInfoJSONRequest(input)
	if err != nil {
		return nil, err
	}

	// Parse body
	var geoInfos map[string]*GeoInfo
	if err := jsonex.Unmarshal(respBody, &geoInfos); err != nil {
		return nil, err
	}

	return geoInfos, nil
}

// SendV2GeoInfoJSONRequest send v2 geo info json request
func (s *GIS) SendV2GeoInfoJSONRequest(input *GetGeoInfoInput) (json.RawMessage, error) {
	// New url
	url := s.Host.String() + "/v2/geoinfo.json?ip=" + input.IP

	// New request
	req, err := http.NewRequest("GET", url, nil)
	req.Header.Set("Accept", "application/json")

	// Set api key
	if len(input.APIKey) > 0 {
		req.Header.Set("X-Api-Key", input.APIKey)
	}

	// Send request
	resp, err := s.Client.Do(req)
	if err != nil {
		return nil, err
	}
	respBody, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()
	if err != nil {
		return nil, err
	}

	return respBody, nil
}

// SendV2GeoInfoRequest send v2 geo info request
func (s *GIS) SendV2GeoInfoRequest(input *GetGeoInfoInput) (*GeoInfo, error) {
	// Send json request
	respBody, err := s.SendV2GeoInfoJSONRequest(input)
	if err != nil {
		return nil, err
	}

	// Parse body
	geoInfo := new(GeoInfo)
	if err := jsonex.Unmarshal(respBody, geoInfo); err != nil {
		return nil, err
	}

	return geoInfo, nil
}
