package gis

import (
	"net/http"
	"net/url"
	"time"
)

// GIS service struct
type GIS struct {
	Client *http.Client
	Host   *url.URL
}

// New get Service instance
func New() *GIS {
	svc := &GIS{
		Client: &http.Client{
			Timeout:   time.Duration(5 * time.Second),
			Transport: &Transport{},
		},
		Host: &url.URL{
			Scheme: "http",
			Host:   endpoint,
		},
	}

	return svc
}

// Transport token transport
type Transport struct{}

// RoundTrip http Transport RoundTrip interface
func (t *Transport) RoundTrip(req *http.Request) (*http.Response, error) {
	if len(apiKey) > 0 {
		req.Header.Set("X-Api-Key", apiKey)
	}
	return http.DefaultTransport.RoundTrip(req)
}
