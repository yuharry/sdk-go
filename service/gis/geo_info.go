package gis

// GeoInfo geo info struct
type GeoInfo struct {
	IP             float64 `json:"ip,omitempty"`
	Latitude       float64 `json:"latitude,omitempty"`
	Longitude      float64 `json:"longitude,omitempty"`
	ContinentName  string  `json:"continent_name,omitempty"`
	ContinentCode  string  `json:"continent_code,omitempty"`
	CountryName    string  `json:"country_name,omitempty"`
	CountryIsoCode string  `json:"country_iso_code,omitempty"`
	CityName       string  `json:"city_name,omitempty"`
	TimeZone       string  `json:"time_zone,omitempty"`
	Coordinates    string  `json:"coordinates,omitempty"`
}

// GeoInfosResource geo infos resource struct
type GeoInfosResource struct {
	GeoInfos []*GeoInfo `json:"geo_infos,omitempty"`
}

// IPsResource ips resource struct
type IPsResource struct {
	IPs []string `json:"ips,omitempty"`
}
