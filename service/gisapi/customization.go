// Code generated by github.com/chouandy/goex/awsex/gen-api/main.go. DO NOT EDIT.

package gisapi

import (
	"github.com/aws/aws-sdk-go-v2/aws"
)

func init() {
	initClient = func(s *GISAPI) {
		// Set default request headers
		s.Handlers.Build.PushBack(func(r *aws.Request) {
			r.HTTPRequest.Header.Add("Accept", "application/json")
			r.HTTPRequest.Header.Add("X-Api-Key", apiKey)
		})
		// Set url
		var url string
		if len(scheme) > 0 {
			url = scheme + "://" + endpoint
		} else {
			url = "https://" + endpoint
		}
		s.Client.Config.EndpointResolver = aws.ResolveWithEndpointURL(url)
	}
}
